package com.example.photogallery.ui.home

import com.example.photogallery.MainCoroutineRule
import com.example.photogallery.data.model.Photo
import com.example.photogallery.data.rest.RestRepository
import com.example.photogallery.utils.AppDispatcher
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.doSuspendableAnswer
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class MainViewModelTest {

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()
    private val repository = mock<RestRepository>()
    private val appDispatcher = AppDispatcher(
        IO = UnconfinedTestDispatcher()
    )

    private lateinit var viewModel: MainViewModel

    @Before
    fun setup() {
        viewModel = MainViewModel(repository, appDispatcher)
    }

    @Test
    fun `verify loading state on loadPhotos`() = runTest {
        val expected = MainUiState.LOADING
        whenever(repository.getPhotos()).doSuspendableAnswer {
            delay(1000)
            emptyList()
        }

        viewModel.loadPhotos()
        Assert.assertEquals(expected, viewModel.uiState.value)
    }

    @Test
    fun `verify success state on loadPhotos`() = runTest {
        val photos = listOf(Photo(1, "Title", "url", "thumbnail"))
        val expected = MainUiState.SUCCESS(photos)
        whenever(repository.getPhotos()).thenReturn(photos)

        viewModel.loadPhotos()
        Assert.assertEquals(expected, viewModel.uiState.value)
    }

    @Test
    fun `verify error state on loadPhotos`() = runTest {
        val expected = MainUiState.ERROR("Error")
        whenever(repository.getPhotos()).thenThrow(RuntimeException("Error"))

        viewModel.loadPhotos()
        Assert.assertEquals(expected, viewModel.uiState.value)
    }


    @Test
    fun `verify showPhotoDetail state on showDetail`() = runTest {
        val photo = Photo(
            1,
            "Title", "url", "thumbnail"
        )

        viewModel.showDetail(photo)

        Assert.assertEquals(photo, viewModel.showPhotoDetails.value)
    }

    @Test
    fun `test showPhotoDetails null on photoDetailShown`() = runTest {
        val photo = Photo(
            1,
            "Title", "url", "thumbnail"
        )
        viewModel.showDetail(photo)

        Assert.assertEquals(photo, viewModel.showPhotoDetails.value)

        viewModel.photoDetailShown()

        Assert.assertNull(viewModel.showPhotoDetails.value)
    }

}