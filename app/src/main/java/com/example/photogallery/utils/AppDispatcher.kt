package com.example.photogallery.utils

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

class AppDispatcher(
    val IO: CoroutineDispatcher = Dispatchers.IO,
    val MAIN: CoroutineDispatcher = Dispatchers.Main,
)
