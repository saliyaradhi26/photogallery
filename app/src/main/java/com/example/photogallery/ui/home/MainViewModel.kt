package com.example.photogallery.ui.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.photogallery.data.model.Photo
import com.example.photogallery.data.rest.RestRepository
import com.example.photogallery.utils.AppDispatcher
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val repository: RestRepository,
    private val appDispatcher: AppDispatcher
) : ViewModel() {

    private val _uiState = MutableStateFlow<MainUiState>(MainUiState.START)
    val uiState: StateFlow<MainUiState> = _uiState.asStateFlow()

    private val _showPhotoDetails = MutableStateFlow<Photo?>(null)
    val showPhotoDetails: StateFlow<Photo?> = _showPhotoDetails.asStateFlow()

    fun loadPhotos() = viewModelScope.launch(appDispatcher.IO) {
        _uiState.emit(MainUiState.LOADING)
        try {
            val photos = repository.getPhotos()
            _uiState.emit(MainUiState.SUCCESS(photos))
        } catch (e: Exception) {
            _uiState.emit(MainUiState.ERROR(e.localizedMessage))
        }
    }

    fun showDetail(photo: Photo) = viewModelScope.launch {
        _showPhotoDetails.emit(photo)
    }

    fun photoDetailShown() = viewModelScope.launch {
        _showPhotoDetails.emit(null)
    }
}

sealed class MainUiState {
    object START : MainUiState()
    object LOADING : MainUiState()
    data class SUCCESS(val photos: List<Photo>) : MainUiState()
    data class ERROR(val message: String) : MainUiState()
}