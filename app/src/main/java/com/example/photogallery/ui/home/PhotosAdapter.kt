package com.example.photogallery.ui.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.photogallery.data.model.Photo
import com.example.photogallery.databinding.ItemPhotoBinding
import com.squareup.picasso.Picasso

class PhotosAdapter : RecyclerView.Adapter<PhotosAdapter.PhotoViewHolder>() {

    var onItemClicked: ((photo: Photo) -> Unit)? = null

    var photos = listOf<Photo>()
        set(value) {
            field = value
            notifyItemRangeChanged(0, itemCount)
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoViewHolder {
        val binding = ItemPhotoBinding.inflate(
            LayoutInflater.from(parent.context),
            parent, false
        )
        return PhotoViewHolder(binding)
    }

    override fun getItemCount() = photos.size

    override fun onBindViewHolder(holder: PhotoViewHolder, position: Int) {
        holder.bind(photos[position])
    }

    inner class PhotoViewHolder(
        private val binding: ItemPhotoBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(photo: Photo) {
            binding.txtTitle.text = photo.title
            Picasso.get()
                .load(photo.thumbnailUrl)
                .into(binding.ImgThumb)

            binding.root.setOnClickListener {
                onItemClicked?.invoke(photo)
            }
        }
    }
}