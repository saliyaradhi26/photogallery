package com.example.photogallery.ui.detail

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.example.photogallery.data.model.Photo
import com.example.photogallery.databinding.ActivityPhotoDetailBinding
import com.squareup.picasso.Picasso
import dagger.hilt.android.AndroidEntryPoint
import org.parceler.Parcels

const val EXTRA_KEY_PHOTO = "extra_photo"

@AndroidEntryPoint
class PhotoDetailActivity : AppCompatActivity() {

    private var _binding: ActivityPhotoDetailBinding? = null
    private val binding: ActivityPhotoDetailBinding
        get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityPhotoDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        init()
        val photo =
            Parcels.unwrap<Photo>(intent.getParcelableExtra(EXTRA_KEY_PHOTO))
        showDetail(photo)
    }

    private fun init() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }


    private fun showDetail(photo: Photo) {
        binding.txtTitle.text = photo.title
        Picasso.get()
            .load(photo.url)
            .into(binding.imgThumb)
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}