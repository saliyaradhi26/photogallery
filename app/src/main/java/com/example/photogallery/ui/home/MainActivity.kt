package com.example.photogallery.ui.home

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.example.photogallery.R
import com.example.photogallery.data.model.Photo
import com.example.photogallery.databinding.ActivityMainBinding
import com.example.photogallery.ui.detail.EXTRA_KEY_PHOTO
import com.example.photogallery.ui.detail.PhotoDetailActivity
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import org.parceler.Parcels

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val viewModel by viewModels<MainViewModel>()

    private var _binding: ActivityMainBinding? = null
    private val binding: ActivityMainBinding
        get() = _binding!!

    private val adapter = PhotosAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()

        viewModel.loadPhotos()

        initObserver()
    }

    private fun initObserver() {

        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {

                viewModel.uiState.collect {
                    binding.loader.isVisible = it == MainUiState.LOADING
                    binding.errorLyt.isVisible = it is MainUiState.ERROR
                    binding.rvPhotos.isVisible = it is MainUiState.SUCCESS

                    if (it is MainUiState.SUCCESS) {
                        val photos = it.photos
                        adapter.photos = photos
                    }
                }
            }
        }

        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.showPhotoDetails.collect { photo ->
                    photo?.let {
                        showDetail(photo = it)
                        viewModel.photoDetailShown()
                    }
                }
            }
        }
    }

    private fun init() {
        binding.rvPhotos.adapter = adapter
        adapter.onItemClicked = {
            viewModel.showDetail(it)
        }
    }

    private fun showDetail(photo: Photo) {
        Intent(this, PhotoDetailActivity::class.java).apply {
            putExtra(EXTRA_KEY_PHOTO, Parcels.wrap(photo))
            startActivity(this)
        }
    }

    private fun showErrorMessage() {
        Toast.makeText(this, R.string.error_message, Toast.LENGTH_SHORT).show()
    }


    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}