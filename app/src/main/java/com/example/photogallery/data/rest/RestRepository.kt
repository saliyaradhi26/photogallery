package com.example.photogallery.data.rest

import com.example.photogallery.data.model.Photo
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class RestRepository @Inject constructor(private val apiService: ApiService) {

    suspend fun getPhotos(): List<Photo> {
        return apiService.getPhotos()
    }
}