package com.example.photogallery.data.model

import org.parceler.Parcel
import org.parceler.ParcelConstructor

@Parcel(Parcel.Serialization.BEAN)
data class Photo @ParcelConstructor constructor(
    val id: Int,
    val title: String,
    val url: String,
    val thumbnailUrl: String
)
