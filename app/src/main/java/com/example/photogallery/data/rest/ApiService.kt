package com.example.photogallery.data.rest

import com.example.photogallery.data.model.Photo
import retrofit2.http.GET

interface ApiService {

    @GET("photos")
    suspend fun getPhotos(): List<Photo>
}